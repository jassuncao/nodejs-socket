# Exercício 1 - Exemplo Socket Nodejs
### @Author: Jônathas Assunção Alves

<p align="center">

[![NodeJs](https://img.shields.io/badge/NodeJS-green.svg)](https://nodejs.org/en/)
[![Express](https://img.shields.io/badge/Express-blue.svg)](https://expressjs.com/pt-br/)
[![Socket.io](https://img.shields.io/badge/Socket.io-red.svg)](https://socket.io/)
</p>

<br/>

Para executar:

``` 
node index.js
``` 
<br/>
<br/>
<br/>

Executar:

> [http://localhost:3000](http://localhost:3000 "localhost:3000")

<br/>
<br/>
<br/>

**Ao enviar alguma String, o servidor retorna uma String invertida.**

